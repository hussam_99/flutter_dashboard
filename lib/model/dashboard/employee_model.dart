class Employee {
  bool check;
  String avatar;
  String name;
  String position;
  String email;
  DateTime date;
  double salary;
  String status;

  Employee({
    required this.check,
    required this.avatar,
    required this.name,
    required this.position,
    required this.email,
    required this.date,
    required this.salary,
    required this.status,
  });
}
