import 'package:flutter/material.dart';

class Folder {
  final String name;
  final IconData icon;
  final List<Folder>? subfolders;

  Folder({
    required this.name,
    required this.icon,
    this.subfolders,
  });
}
