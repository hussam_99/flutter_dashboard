import 'package:flutter/material.dart';

class MenuModel {
  final String title;
  final IconData icon;
  final List<MenuModel>? children;

  MenuModel({
    required this.title,
    required this.icon,
    this.children,
  });
}
