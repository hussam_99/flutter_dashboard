import 'package:dashboard_test/model/dashboard/employee_model.dart';

class TableData {
  final employees = <Employee>[
    Employee(
          check: false,
          avatar: 'assets/images/hussam-pic.png',
          name: 'Hussam',
          position: 'Manager',
          email: 'hussam@example.com',
          date: DateTime.now(),
          salary: 5000,
          status: 'current'),
      Employee(
          check: false,
          avatar: 'assets/images/hussam-pic.png',
          name: 'Hussam',
          position: 'Manager',
          email: 'hussam@example.com',
          date: DateTime.now(),
          salary: 5000,
          status: 'resigned'),
      Employee(
          check: false,
          avatar: 'assets/images/hussam-pic.png',
          name: 'Hussam',
          position: 'Manager',
          email: 'hussam@example.com',
          date: DateTime.now(),
          salary: 5000,
          status: 'professional'),
    

  ];
}
