import 'package:dashboard_test/model/dashboard/folder_model.dart';
import 'package:flutter/material.dart';

class RightMenuData {
  final menu = <Folder>[
    Folder(name: "عنوان الاختصاص", icon: Icons.folder_open, subfolders: [
      Folder(
        name: "فرع الأختصاص",
        icon: Icons.folder,
      ),
      Folder(name: "فرع الأختصاص", icon: Icons.folder),
      Folder(name: "فرع الأختصاص", icon: Icons.folder),
      Folder(name: "فرع الأختصاص", icon: Icons.folder, subfolders: [
        Folder(name: "فرع الأختصاص", icon: Icons.folder),
        Folder(name: "فرع الأختصاص", icon: Icons.folder),
        Folder(name: "فرع الأختصاص", icon: Icons.folder, subfolders: [
          Folder(name: "فرع الأختصاص", icon: Icons.folder),
          Folder(name: "فرع الأختصاص", icon: Icons.folder),
          Folder(name: "فرع الأختصاص", icon: Icons.folder, subfolders: [
            Folder(name: "فرع الأختصاص", icon: Icons.folder),
            Folder(name: "فرع الأختصاص", icon: Icons.folder),
            Folder(name: "فرع الأختصاص", icon: Icons.folder),
          ]),
        ]),
      ]),
      Folder(name: "فرع الأختصاص", icon: Icons.folder),
      Folder(name: "فرع الأختصاص", icon: Icons.folder),
      Folder(name: "فرع الأختصاص", icon: Icons.folder),
    ])
  ];
}
