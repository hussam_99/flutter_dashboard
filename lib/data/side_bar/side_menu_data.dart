import 'package:dashboard_test/model/side_bar/menu_model.dart';
import 'package:flutter/material.dart';

class SideMenuData {
  final menu = <MenuModel>[
    MenuModel(title: "MAIN", icon: Icons.menu, children: [
      MenuModel(
        title: "الاحصائيات",
        icon: Icons.show_chart_sharp,
      ),
      MenuModel(
        title: "الفحص الطبي",
        icon: Icons.supervised_user_circle_outlined,
      ),
      MenuModel(
        title: "الحجوزات و المواعيد",
        icon: Icons.calendar_month,
      ),
      MenuModel(
        title: "المرضى",
        icon: Icons.sports_gymnastics_rounded,
      ),
      MenuModel(
        title: "الرسائل",
        icon: Icons.notifications_none_outlined,
      ),
    ]),
    MenuModel(title: "1الرئيسية", icon: Icons.menu, children: [
      MenuModel(
        title: "الرئيسية",
        icon: Icons.menu,
      ),
      MenuModel(
        title: "الرئيسية",
        icon: Icons.menu,
      ),
      MenuModel(title: "الرئيسية", icon: Icons.menu, children: [
        MenuModel(
          title: "الرئيسية",
          icon: Icons.menu,
        ),
        MenuModel(
          title: "الرئيسية",
          icon: Icons.menu,
        ),
        MenuModel(title: "الرئيسية", icon: Icons.menu, children: [
          MenuModel(
            title: "الرئيسية",
            icon: Icons.menu,
          ),
          MenuModel(
            title: "الرئيسية",
            icon: Icons.menu,
          ),
          MenuModel(
            title: "الرئيسية",
            icon: Icons.menu,
          ),
          MenuModel(
            title: "الرئيسية",
            icon: Icons.menu,
          ),
          MenuModel(
            title: "الرئيسية",
            icon: Icons.menu,
          ),
          MenuModel(
            title: "الرئيسية",
            icon: Icons.menu,
          ),
        ]),
      ]),
    ]),
    ];
}
