import 'package:flutter/cupertino.dart';

class AppColors {
  static Color mainColor = const Color(0xFF707070);
  static Color whiteColor = const Color.fromARGB(255, 255, 255, 255);
  static Color greenColor = const Color(0xFF28C76F);
  static Color redColor = const Color(0xFFFE797A);
  static Color oragneColor = const Color(0xFFFF9F43);
  static Color blueColor = const Color(0xFF7367F0);
}
