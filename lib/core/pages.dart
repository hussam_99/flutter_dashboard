// ignore_for_file: depend_on_referenced_packages

import 'package:dashboard_test/core/app_route.dart';
import 'package:dashboard_test/views/home_view/home_page.dart';
import 'package:get/get.dart';

List<GetPage<dynamic>> listOfPages = [
  GetPage(
    name: AppRoute.homePage,
    page: () => HomePage(),
  ),
];
