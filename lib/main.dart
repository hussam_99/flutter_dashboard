// ignore_for_file: depend_on_referenced_packages

import 'package:dashboard_test/views/home_view/home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
          fontFamily:
              Get.locale?.languageCode == "ar" ? "Segoe UI" : "Montserrat",
          textSelectionTheme: TextSelectionThemeData(
              selectionHandleColor: Colors.white.withOpacity(0.5))),
      fallbackLocale: const Locale('en', 'US'),
      debugShowCheckedModeBanner: false,
      title: 'Sadeem Dashboard',
      home: HomePage(),
    );
  }
}
