// ignore_for_file: sized_box_for_whitespace, depend_on_referenced_packages

import 'package:dashboard_test/controllers/home_controller.dart';
import 'package:dashboard_test/core/app_colors.dart';
import 'package:dashboard_test/widgets/dashboard/right_bar/right_bar_widget.dart';
import 'package:dashboard_test/widgets/dashboard/table/table_widget.dart';
import 'package:dashboard_test/widgets/dashboard/top_bar/top_bars.dart';
import 'package:dashboard_test/widgets/side_bar/side_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DesktopView extends StatelessWidget {
  DesktopView({
    super.key,
    required this.controller,
  });

  final HomeController controller;
  final homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 200,
          decoration: BoxDecoration(
            color: AppColors.whiteColor,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 4.0,
                offset: const Offset(0, 2),
              ),
            ],
          ),
          child: SideBar(),
        ),
        Column(
          children: [
            Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                width: MediaQuery.of(context).size.width - 220,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    /* const SizedBox(
                      height: 100,
                      child: Column(
                        children: [
                          UserBarWidget(),
                          BowserBarWidget(),
                        ],
                      ),
                    ),*/
                    Scrollbar(
                      controller: controller.dashboardScrollBar,
                      thumbVisibility: true,
                      child: SingleChildScrollView(
                        controller: controller.dashboardScrollBar,
                        scrollDirection: Axis.horizontal,
                        child: Column(
                          children: [
                            TopBars(
                              width: MediaQuery.of(context).size.width - 220,
                            ),
                            // const DesktopBars(),
                            SizedBox(
                              width: MediaQuery.of(context).size.width - 220,
                              height: MediaQuery.of(context).size.height - 120,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  TableWidget(
                                    employees:
                                        homeController.employeesMenu.employees,
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  RightBarWidget(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ],
        ),
      ],
    );
  }
}
