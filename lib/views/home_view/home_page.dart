// ignore_for_file: unnecessary_import, depend_on_referenced_packages

import 'package:dashboard_test/controllers/home_controller.dart';
import 'package:dashboard_test/util/responsive.dart';
import 'package:dashboard_test/views/home_view/desktop_view.dart';
import 'package:dashboard_test/views/home_view/mobile_view.dart';
import 'package:dashboard_test/views/home_view/tablet_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  HomePage({super.key});
  @override
  final controller = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    final isDesktop = Responsive.isDesktop(context);
    final isTablet = Responsive.isTablet(context);
    final isMobile = Responsive.isMobile(context);
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          isDesktop
              ? DesktopView(controller: controller)
              : isTablet
                  ? TabletView(controller: controller)
                  : isMobile
                      ? MobileView(controller: controller)
                      : const SizedBox(),
          // Text(isDesktop
          //     ? "isDesktop"
          //     : isTablet
          //         ? "isTablet"
          //         : isMobile
          //             ? "isMobile"
          //             : ""),
        ],
      )),
    );
  }
}
