// ignore_for_file: depend_on_referenced_packages

import 'package:dashboard_test/data/dashboard/right_bar/right_menu_data.dart';
import 'package:dashboard_test/data/dashboard/table/table_data.dart';
import 'package:dashboard_test/data/side_bar/side_menu_data.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  ScrollController rightBarScrollBar = ScrollController();
  ScrollController dashboardScrollBar = ScrollController();
  RightMenuData rightMenu = RightMenuData();
  TableData employeesMenu = TableData();
  SideMenuData sideMenu = SideMenuData();

  RxBool isList = true.obs;
}
