// ignore_for_file: depend_on_referenced_packages

import 'package:dashboard_test/controllers/home_controller.dart';
import 'package:dashboard_test/core/app_colors.dart';
import 'package:dashboard_test/widgets/dashboard/right_bar/folder_tree_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RightBarWidget extends StatelessWidget {
  final controller = Get.put(HomeController());

  RightBarWidget({super.key});
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2),
            color: AppColors.whiteColor,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 4.0,
                offset: const Offset(0, 2),
              ),
            ],
          ),
          // margin: const EdgeInsets.symmetric(
          //   horizontal: 10,
          // ),
          width: 200,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Container(
                height: 30,
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 0.2),
                    borderRadius: BorderRadius.circular(4)),
                width: 300,
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        controller: TextEditingController(),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          alignLabelWithHint: true,
                          hintText: 'Search',
                          hintStyle: TextStyle(
                              fontSize: 15, color: AppColors.mainColor),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Scrollbar(
                controller: controller.rightBarScrollBar,
                thumbVisibility: true,
                child: SingleChildScrollView(
                  controller: controller.rightBarScrollBar,
                  scrollDirection: Axis.horizontal,
                  child: SizedBox(
                    width: 200,
                    height: MediaQuery.of(context).size.height - 100,
                    child: FolderTreeWidget(
                      folders: controller.rightMenu.menu,
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
