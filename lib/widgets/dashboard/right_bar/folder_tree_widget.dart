// ignore_for_file: depend_on_referenced_packages
import 'package:dashboard_test/core/app_colors.dart';
import 'package:dashboard_test/model/dashboard/folder_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FolderTreeWidget extends StatelessWidget {
  final List<Folder> folders;
  final double spacing; // Adjust spacing based on depth

  const FolderTreeWidget(
      {super.key, required this.folders, this.spacing = 16.0});

  Widget _buildFolder(Folder folder, int depth) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(right: depth * spacing),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(folder.name,
                  style: TextStyle(fontSize: 15, color: AppColors.mainColor)),
              const SizedBox(width: 2),
              Icon(
                folder.icon,
                color: AppColors.mainColor,
              ),
            ],
          ),
        ),
        if (folder.subfolders != null && folder.subfolders!.isNotEmpty)
          ...folder.subfolders!.map((child) => _buildFolder(child, depth + 1)),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: folders.length,
      itemBuilder: (context, index) {
        return _buildFolder(folders[index], 0);
      },
    );
  }
}
