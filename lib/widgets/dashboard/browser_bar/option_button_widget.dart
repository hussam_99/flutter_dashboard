import 'package:dashboard_test/core/app_colors.dart';
import 'package:flutter/material.dart';

class OptionWidget extends StatelessWidget {
  final IconData icon;
  final String optionName;

  const OptionWidget({
    super.key,
    required this.icon,
    required this.optionName,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 3),
      child: Column(
        children: [
          Icon(
            icon,
            size: 20,
            color: AppColors.mainColor,
          ),
          Text(
            optionName,
            style: TextStyle(
              fontSize: 12,
              color: AppColors.mainColor,
            ),
          ),
        ],
      ),
    );
  }
}
