import 'package:dashboard_test/core/app_colors.dart';
import 'package:dashboard_test/widgets/dashboard/browser_bar/option_button_widget.dart';
import 'package:flutter/material.dart';

class BowserBarWidget extends StatelessWidget {
  final double? width;

  const BowserBarWidget({super.key, this.width});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        width: width ?? MediaQuery.of(context).size.width - 220,
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.all(1),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: AppColors.whiteColor,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 4,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: const SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          reverse: true,
          child: 
          
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              OptionWidget(
                icon: Icons.help,
                optionName: "مساعدة",
              ),
              OptionWidget(
                icon: Icons.update,
                optionName: "تحديث",
              ),
              OptionWidget(
                icon: Icons.settings,
                optionName: "الإعدادات",
              ),
              OptionWidget(
                icon: Icons.arrow_downward,
                optionName: "استيراد",
              ),
              OptionWidget(
                icon: Icons.arrow_upward,
                optionName: "تصدير",
              ),
              OptionWidget(
                icon: Icons.print,
                optionName: "طباعة",
              ),
              OptionWidget(
                icon: Icons.delete,
                optionName: "حذف خدمة",
              ),
              OptionWidget(
                icon: Icons.edit,
                optionName: "تعديل خدمة",
              ),
              OptionWidget(
                icon: Icons.add,
                optionName: "خدمة جديدة",
              ),
              OptionWidget(
                icon: Icons.folder,
                optionName: "حذف مجموعة",
              ),
              OptionWidget(
                icon: Icons.folder,
                optionName: "تعديل مجموعة",
              ),
              OptionWidget(
                icon: Icons.folder,
                optionName: "مجموعة جديدة",
              ),
            ],
          ),
        
        
        ),
      ),
    );
  }
}
