// ignore_for_file: prefer_const_constructors, unnecessary_import, depend_on_referenced_packages, prefer_const_literals_to_create_immutables

import 'package:dashboard_test/controllers/home_controller.dart';
import 'package:dashboard_test/core/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class DisplayBarWidget extends StatelessWidget {
  final TextEditingController searchController;
  final String selectedSortBy;
  final ValueChanged<String>? onSortByChanged;
  final bool isGridSelected;
  final ValueChanged<bool>? onViewChanged;
  final controller = Get.put(HomeController());

  DisplayBarWidget({
    super.key,
    required this.searchController,
    required this.selectedSortBy,
    this.onSortByChanged,
    required this.isGridSelected,
    this.onViewChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: AppColors.whiteColor,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 4.0,
            offset: const Offset(0, 2),
          ),
        ],
      ),
      child: Obx(
        () {
          return Row(
            children: [
              const Text(
                "Show",
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                  padding: EdgeInsets.all(1),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                        color: Colors.black,
                        width: 0.5,
                      )),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          controller.isList.value = true;
                        },
                        child: Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: controller.isList.value == true
                                ? AppColors.redColor
                                : AppColors.whiteColor,
                          ),
                          child: Image.asset(
                            "assets/icons/list_view.png",
                            color: controller.isList.value
                                ? AppColors.whiteColor
                                : AppColors.mainColor,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          controller.isList.value = false;
                        },
                        child: Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: controller.isList.value != true
                                ? AppColors.redColor
                                : AppColors.whiteColor,
                          ),
                          child: Image.asset(
                            "assets/icons/grid_view.png",
                            color: controller.isList.value != true
                                ? AppColors.whiteColor
                                : AppColors.mainColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: 30,
                padding: EdgeInsets.symmetric(horizontal: 12),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 0.2),
                    borderRadius: BorderRadius.circular(4)),
                child: Row(
                  children: [
                    Text(
                      "Sort By:",
                      style:
                          TextStyle(fontSize: 15, color: AppColors.mainColor),
                    ),
                    DropdownButton<String>(
                      underline: const SizedBox(),
                      value: selectedSortBy,
                      onChanged: (d) {}, //onSortByChanged,
                      items: [
                        DropdownMenuItem(
                          value: 'All',
                          child: Text(
                            'All Categories',
                            style: TextStyle(
                                fontSize: 15, color: AppColors.mainColor),
                          ),
                        ),
                        DropdownMenuItem(
                          value: 'Name',
                          child: Text(
                            'Sort by Name',
                            style: TextStyle(
                                fontSize: 15, color: AppColors.mainColor),
                          ),
                        ),
                        DropdownMenuItem(
                          value: 'Date',
                          child: Text(
                            'Sort by Date',
                            style: TextStyle(
                                fontSize: 15, color: AppColors.mainColor),
                          ),
                        ),
                        DropdownMenuItem(
                          value: 'Salary',
                          child: Text(
                            'Sort by Salary',
                            style: TextStyle(
                                fontSize: 15, color: AppColors.mainColor),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Spacer(),
              Container(
                height: 30,
                padding: EdgeInsets.symmetric(horizontal: 15),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 0.2),
                    borderRadius: BorderRadius.circular(4)),
                width: 200,
                child: Row(
                  children: [
                    Text(
                      "Search",
                      style:
                          TextStyle(fontSize: 15, color: AppColors.mainColor),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: TextField(
                        controller: searchController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          alignLabelWithHint: true,
                          hintText: 'Search',
                          hintStyle: TextStyle(
                              fontSize: 15, color: AppColors.mainColor),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
