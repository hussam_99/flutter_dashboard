
/*
class MobileBarsWidget extends StatelessWidget {
  const MobileBarsWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.amber,
      width: 1000,
      height: 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColors.whiteColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      UserBarButton(image: 'assets/icons/flag.png'),
                      UserBarButton(image: 'assets/icons/topbar_apps.png'),
                      UserBarButton(image: 'assets/icons/full_screen.png'),
                      UserBarButton(image: 'assets/icons/bell.png'),
                    ],
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                UserAvatar(
                  imageUrl: 'assets/images/hussam-pic.png',
                  isOnline: true,
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColors.whiteColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                OptionWidget(
                  icon: Icons.help,
                  optionName: "مساعدة",
                ),
                OptionWidget(
                  icon: Icons.update,
                  optionName: "تحديث",
                ),
                OptionWidget(
                  icon: Icons.settings,
                  optionName: "الإعدادات",
                ),
                OptionWidget(
                  icon: Icons.arrow_downward,
                  optionName: "استيراد",
                ),
                OptionWidget(
                  icon: Icons.arrow_upward,
                  optionName: "تصدير",
                ),
                OptionWidget(
                  icon: Icons.print,
                  optionName: "طباعة",
                ),
                OptionWidget(
                  icon: Icons.delete,
                  optionName: "حذف خدمة",
                ),
                OptionWidget(
                  icon: Icons.edit,
                  optionName: "تعديل خدمة",
                ),
                OptionWidget(
                  icon: Icons.add,
                  optionName: "خدمة جديدة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "حذف مجموعة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "تعديل مجموعة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "مجموعة جديدة",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
*/
//###############################

/*
class DesktopBars extends StatelessWidget {
  const DesktopBars({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 220,
      height: 100,
      // color: Colors.amber,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColors.whiteColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      UserBarButton(image: 'assets/icons/flag.png'),
                      UserBarButton(image: 'assets/icons/topbar_apps.png'),
                      UserBarButton(image: 'assets/icons/full_screen.png'),
                      UserBarButton(image: 'assets/icons/bell.png'),
                    ],
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                UserAvatar(
                  imageUrl: 'assets/images/hussam-pic.png',
                  isOnline: true,
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColors.whiteColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                OptionWidget(
                  icon: Icons.help,
                  optionName: "مساعدة",
                ),
                OptionWidget(
                  icon: Icons.update,
                  optionName: "تحديث",
                ),
                OptionWidget(
                  icon: Icons.settings,
                  optionName: "الإعدادات",
                ),
                OptionWidget(
                  icon: Icons.arrow_downward,
                  optionName: "استيراد",
                ),
                OptionWidget(
                  icon: Icons.arrow_upward,
                  optionName: "تصدير",
                ),
                OptionWidget(
                  icon: Icons.print,
                  optionName: "طباعة",
                ),
                OptionWidget(
                  icon: Icons.delete,
                  optionName: "حذف خدمة",
                ),
                OptionWidget(
                  icon: Icons.edit,
                  optionName: "تعديل خدمة",
                ),
                OptionWidget(
                  icon: Icons.add,
                  optionName: "خدمة جديدة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "حذف مجموعة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "تعديل مجموعة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "مجموعة جديدة",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
*/

//##########################

/*
class TabletBars extends StatelessWidget {
  const TabletBars({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1000,
      // color: Colors.amber,
      height: 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColors.whiteColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      UserBarButton(image: 'assets/icons/flag.png'),
                      UserBarButton(image: 'assets/icons/topbar_apps.png'),
                      UserBarButton(image: 'assets/icons/full_screen.png'),
                      UserBarButton(image: 'assets/icons/bell.png'),
                    ],
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                UserAvatar(
                  imageUrl: 'assets/images/hussam-pic.png',
                  isOnline: true,
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColors.whiteColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                OptionWidget(
                  icon: Icons.help,
                  optionName: "مساعدة",
                ),
                OptionWidget(
                  icon: Icons.update,
                  optionName: "تحديث",
                ),
                OptionWidget(
                  icon: Icons.settings,
                  optionName: "الإعدادات",
                ),
                OptionWidget(
                  icon: Icons.arrow_downward,
                  optionName: "استيراد",
                ),
                OptionWidget(
                  icon: Icons.arrow_upward,
                  optionName: "تصدير",
                ),
                OptionWidget(
                  icon: Icons.print,
                  optionName: "طباعة",
                ),
                OptionWidget(
                  icon: Icons.delete,
                  optionName: "حذف خدمة",
                ),
                OptionWidget(
                  icon: Icons.edit,
                  optionName: "تعديل خدمة",
                ),
                OptionWidget(
                  icon: Icons.add,
                  optionName: "خدمة جديدة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "حذف مجموعة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "تعديل مجموعة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "مجموعة جديدة",
                ),
              ],
            ),
          ),
        ],
      ),

      // child: Column(
      //   children: [
      //     UserBarWidget(),
      //     BowserBarWidget(),
      //   ],
      // ),
    );
  }
}
*/

