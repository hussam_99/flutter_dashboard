// ignore_for_file: sized_box_for_whitespace

import 'package:dashboard_test/core/app_colors.dart';
import 'package:dashboard_test/widgets/dashboard/browser_bar/option_button_widget.dart';
import 'package:dashboard_test/widgets/dashboard/user_bar/user_avatar.dart';
import 'package:dashboard_test/widgets/dashboard/user_bar/user_bar_widget.dart';
import 'package:flutter/material.dart';

class TopBars extends StatelessWidget {
  final double width;
  const TopBars({super.key, required this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width, // MediaQuery.of(context).size.width - 220,
      height: 100,
      // color: Colors.amber,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColors.whiteColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      UserBarButton(image: 'assets/icons/flag.png'),
                      UserBarButton(image: 'assets/icons/topbar_apps.png'),
                      UserBarButton(image: 'assets/icons/full_screen.png'),
                      UserBarButton(image: 'assets/icons/bell.png'),
                    ],
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                UserAvatar(
                  imageUrl: 'assets/images/hussam-pic.png',
                  isOnline: true,
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: AppColors.whiteColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                OptionWidget(
                  icon: Icons.help,
                  optionName: "مساعدة",
                ),
                OptionWidget(
                  icon: Icons.update,
                  optionName: "تحديث",
                ),
                OptionWidget(
                  icon: Icons.settings,
                  optionName: "الإعدادات",
                ),
                OptionWidget(
                  icon: Icons.arrow_downward,
                  optionName: "استيراد",
                ),
                OptionWidget(
                  icon: Icons.arrow_upward,
                  optionName: "تصدير",
                ),
                OptionWidget(
                  icon: Icons.print,
                  optionName: "طباعة",
                ),
                OptionWidget(
                  icon: Icons.delete,
                  optionName: "حذف خدمة",
                ),
                OptionWidget(
                  icon: Icons.edit,
                  optionName: "تعديل خدمة",
                ),
                OptionWidget(
                  icon: Icons.add,
                  optionName: "خدمة جديدة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "حذف مجموعة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "تعديل مجموعة",
                ),
                OptionWidget(
                  icon: Icons.folder,
                  optionName: "مجموعة جديدة",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
