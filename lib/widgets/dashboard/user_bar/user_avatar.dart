import 'package:dashboard_test/core/app_colors.dart';
import 'package:flutter/material.dart';

class UserAvatar extends StatelessWidget {
  final String imageUrl;
  final bool isOnline;

  const UserAvatar({
    super.key,
    required this.imageUrl,
    required this.isOnline,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CircleAvatar(
          radius: 20,
          backgroundImage: AssetImage(imageUrl),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: Container(
            width: 14,
            height: 14,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: isOnline ? AppColors.greenColor : AppColors.mainColor,
              border: Border.all(
                color: AppColors.whiteColor,
                width: 2,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
