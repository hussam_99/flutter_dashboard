// ignore_for_file: prefer_const_constructors

import 'package:dashboard_test/core/app_colors.dart';
import 'package:dashboard_test/widgets/dashboard/user_bar/user_avatar.dart';
import 'package:flutter/material.dart';

class UserBarWidget extends StatelessWidget {
  final double? width;
  const UserBarWidget({super.key, this.width});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        // width: MediaQuery.of(context).size.width - 220,
        width: width ?? MediaQuery.of(context).size.width - 220,
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.all(2),
        decoration: BoxDecoration(
          color: AppColors.whiteColor,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 4,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: SingleChildScrollView(
          reverse: true,
          scrollDirection: Axis.horizontal,
          child: const 
          
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    UserBarButton(image: 'assets/icons/flag.png'),
                    UserBarButton(image: 'assets/icons/topbar_apps.png'),
                    UserBarButton(image: 'assets/icons/full_screen.png'),
                    UserBarButton(image: 'assets/icons/bell.png'),
                  ],
                ),
              ),
              SizedBox(
                width: 30,
              ),
              UserAvatar(
                imageUrl: 'assets/images/hussam-pic.png',
                isOnline: true,
              ),
            ],
          ),
        
        
        
        ),
      ),
    );
  }
}

class UserBarButton extends StatelessWidget {
  final String image;
  const UserBarButton({
    super.key,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 3),
      child: SizedBox(
        width: 20,
        child: Image.asset(image),
      ),
    );
  }
}
// 
