import 'package:dashboard_test/core/app_colors.dart';
import 'package:dashboard_test/model/dashboard/employee_model.dart';
import 'package:dashboard_test/util/responsive.dart';
import 'package:dashboard_test/widgets/dashboard/display_bar/display_bar_widget.dart';
import 'package:flutter/material.dart';

class TableWidget extends StatelessWidget {
  const TableWidget({
    super.key,
    required this.employees,
  });
  final List<Employee> employees;
  @override
  Widget build(BuildContext context) {
    final isDesktop = Responsive.isDesktop(context);
    return Expanded(
        child: SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 4.0,
            offset: const Offset(0, 2),
          ),
        ], color: AppColors.whiteColor, borderRadius: BorderRadius.circular(5)),
        child: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10,
                  ),
                  child: Text('الخدمات الطبية',
                      style: TextStyle(
                        fontSize: 15,
                        color: AppColors.mainColor,
                      )),
                ),
              ],
            ),
            const Divider(),
            DisplayBarWidget(
              isGridSelected: true,
              searchController: TextEditingController(),
              selectedSortBy: "All",
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              // height: MediaQuery.of(context).size.height - 250,
              width: 1000,
              decoration: BoxDecoration(
                color: AppColors.whiteColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 4.0,
                    offset: const Offset(0, 2),
                  ),
                ],
              ),
              child: DataTable(
                columnSpacing: isDesktop ? 20 : 10,
                horizontalMargin: 5,
                sortColumnIndex: 1,
                columns: <DataColumn>[
                  DataColumn(
                      label: Text(
                    '',
                    style: TextStyle(
                      color: AppColors.mainColor,
                    ),
                  )),
                  DataColumn(
                      label: Text(
                    'Name',
                    style: TextStyle(
                      color: AppColors.mainColor,
                    ),
                  )),
                  DataColumn(
                      label: Text(
                    'Email',
                    style: TextStyle(
                      color: AppColors.mainColor,
                    ),
                  )),
                  DataColumn(
                      label: Text(
                    'Date',
                    style: TextStyle(
                      color: AppColors.mainColor,
                    ),
                  )),
                  DataColumn(
                      label: Text(
                    'Salary',
                    style: TextStyle(
                      color: AppColors.mainColor,
                    ),
                  )),
                  DataColumn(
                      label: Text(
                    'Status',
                    style: TextStyle(
                      color: AppColors.mainColor,
                    ),
                  )),
                  DataColumn(
                      label: Text(
                    'Actions',
                    style: TextStyle(
                      color: AppColors.mainColor,
                    ),
                  )),
                ],
                rows: fetchEmployees(employees: employees),
              ),
            ),
          ],
        ),
      ),
    ));
  }

  fetchEmployees({required List<Employee> employees}) {
    return employees.map((employee) {
      return DataRow(
        cells: <DataCell>[
          DataCell(Transform.scale(
              scale: 0.8,
              child: Checkbox(value: employee.check, onChanged: (value) {}))),
          DataCell(
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage(employee.avatar),
                  radius: 20,
                ),
                const SizedBox(
                  width: 4,
                ),
                Text(
                  '${employee.name}\n${employee.position}',
                  style: TextStyle(
                      fontSize: 12,
                      overflow: TextOverflow.ellipsis,
                      color: AppColors.mainColor),
                ),
              ],
            ),
          ),
          DataCell(
            Text(
              employee.email,
              style: TextStyle(fontSize: 12, color: AppColors.mainColor),
            ),
          ),
          DataCell(
            Text(
              '${employee.date.day}/${employee.date.month}/${employee.date.year}',
              style: TextStyle(fontSize: 12, color: AppColors.mainColor),
            ),
          ),
          DataCell(
            Text(
              '\$${employee.salary.toStringAsFixed(2)}',
              style: TextStyle(fontSize: 12, color: AppColors.mainColor),
            ),
          ),
          DataCell(
            Container(
              height: 20,
              alignment: Alignment.center,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: employee.status == 'current'
                    ? AppColors.blueColor.withOpacity(0.2)
                    : employee.status == 'professional'
                        ? AppColors.greenColor.withOpacity(0.2)
                        : employee.status == 'resigned'
                            ? AppColors.oragneColor.withOpacity(0.2)
                            : AppColors.redColor.withOpacity(0.2),
              ),
              child: Text(
                employee.status,
                style: TextStyle(
                  fontSize: 10,
                  color: employee.status == 'current'
                      ? AppColors.blueColor
                      : employee.status == 'professional'
                          ? AppColors.greenColor
                          : employee.status == 'resigned'
                              ? AppColors.oragneColor
                              : AppColors.redColor,
                ),
              ),
            ),
          ),
          DataCell(Row(
            children: <Widget>[
              Transform.scale(
                  scale: 0.8,
                  child: IconButton(
                      icon: Icon(
                        Icons.more_vert,
                        color: AppColors.blueColor,
                      ),
                      onPressed: () {})),
              Transform.scale(
                  scale: 0.8,
                  child: Checkbox(
                      value: employee.check,
                      activeColor: AppColors.blueColor,
                      checkColor: AppColors.blueColor,
                      onChanged: (value) {})),
            ],
          )),
        ],
      );
    }).toList();
  }
}
