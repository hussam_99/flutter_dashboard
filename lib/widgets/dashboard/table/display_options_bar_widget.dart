// ignore_for_file: depend_on_referenced_packages

import 'package:dashboard_test/controllers/home_controller.dart';
import 'package:dashboard_test/core/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DisplayOptionsBarWidget extends StatelessWidget {
  final TextEditingController searchController;
  final String selectedSortBy;
  final ValueChanged<String>? onSortByChanged;
  final bool isGridSelected;
  final ValueChanged<bool>? onViewChanged;
  DisplayOptionsBarWidget({
    super.key,
    required this.searchController,
    required this.selectedSortBy,
    this.onSortByChanged,
    required this.isGridSelected,
    this.onViewChanged,
  });
  final controller = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Row(
      children: [
        Text(
          "Show",
          style: TextStyle(
            color: AppColors.mainColor,
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            padding: const EdgeInsets.all(1),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: Colors.black,
                  width: 0.5,
                )),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    controller.isList.value = true;
                  },
                  child: Container(
                    padding: const EdgeInsets.all(7),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: controller.isList.value == true
                          ? AppColors.redColor
                          : AppColors.whiteColor,
                    ),
                    child: Image.asset(
                      "assets/icons/list_view.png",
                      color: controller.isList.value
                          ? AppColors.whiteColor
                          : AppColors.mainColor,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    controller.isList.value = false;
                  },
                  child: Container(
                    padding: const EdgeInsets.all(7),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: controller.isList.value != true
                          ? AppColors.redColor
                          : AppColors.whiteColor,
                    ),
                    child: Image.asset(
                      "assets/icons/grid_view.png",
                      color: controller.isList.value != true
                          ? AppColors.whiteColor
                          : AppColors.mainColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          height: 50,
          padding: const EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 0.2),
              borderRadius: BorderRadius.circular(4)),
          child: Row(
            children: [
              Text(
                "Sort By:",
                style: TextStyle(fontSize: 15, color: AppColors.mainColor),
              ),
              DropdownButton<String>(
                underline: const SizedBox(),
                value: selectedSortBy,
                onChanged: (d) {}, //onSortByChanged,
                items: [
                  DropdownMenuItem(
                    value: 'All',
                    child: Text(
                      'All Categories',
                      style:
                          TextStyle(fontSize: 15, color: AppColors.mainColor),
                    ),
                  ),
                  DropdownMenuItem(
                    value: 'Name',
                    child: Text(
                      'Sort by Name',
                      style:
                          TextStyle(fontSize: 15, color: AppColors.mainColor),
                    ),
                  ),
                  DropdownMenuItem(
                    value: 'Date',
                    child: Text(
                      'Sort by Date',
                      style:
                          TextStyle(fontSize: 15, color: AppColors.mainColor),
                    ),
                  ),
                  DropdownMenuItem(
                    value: 'Salary',
                    child: Text(
                      'Sort by Salary',
                      style:
                          TextStyle(fontSize: 15, color: AppColors.mainColor),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        const Spacer(),
        Container(
          height: 50,
          padding: const EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 0.2),
              borderRadius: BorderRadius.circular(4)),
          width: 300,
          child: Row(
            children: [
              Text(
                "Search",
                style: TextStyle(fontSize: 15, color: AppColors.mainColor),
              ),
              const SizedBox(
                width: 20,
              ),
              Expanded(
                child: TextField(
                  controller: searchController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    alignLabelWithHint: true,
                    hintText: 'Search',
                    hintStyle:
                        TextStyle(fontSize: 22, color: AppColors.mainColor),
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(width: 16),
        const SizedBox(width: 16),
      ],
    ));
  }
}
