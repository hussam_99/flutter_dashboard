// ignore_for_file: depend_on_referenced_packages

import 'package:dashboard_test/controllers/home_controller.dart';
import 'package:dashboard_test/core/app_colors.dart';
import 'package:dashboard_test/model/side_bar/menu_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class SideBar extends StatelessWidget {
  SideBar({super.key});
  final homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      height: MediaQuery.of(context).size.height + 10,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 20, top: 20, bottom: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 30,
                  height: 30,
                  child: Image.asset("assets/images/logo.png"),
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "Ezar Group",
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: AppColors.redColor,
                      fontSize: 15),
                ),
              ],
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height - 100,
            child: ListView.builder(
              shrinkWrap: true,
              itemBuilder: (context, index) => MenuItemWidget(
                menuModel: homeController.sideMenu.menu.elementAt(index),
              ),
              itemCount: homeController.sideMenu.menu.length,
            ),
          ),
        ],
      ),
    );
  }
}

class MenuItemWidget extends StatelessWidget {
  final MenuModel menuModel;
  const MenuItemWidget({
    super.key,
    required this.menuModel,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text(
              menuModel.title,
              style: TextStyle(
                color: AppColors.mainColor,
                fontSize: 10,
              ),
            ),
          ),
          menuModel.children != null && menuModel.children!.isNotEmpty
              ? SubSectionWidget(
                  folders: menuModel.children!,
                )
              /*ListView.builder(
                  itemCount: menuModel.children!.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) => Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.symmetric(vertical: 4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          menuModel.children!.elementAt(index).icon,
                          color: AppColors.mainColor,
                          size: 15,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          menuModel.children!.elementAt(index).title,
                          style: TextStyle(
                            fontSize: 12,
                            color: AppColors.mainColor,
                          ),
                        )
                      ],
                    ),
                  ),
                )
              */
              : const SizedBox()
        ],
      ),
    );
  }
}

class SubSectionWidget extends StatelessWidget {
  final List<MenuModel> folders;
  final double spacing; // Adjust spacing based on depth

  const SubSectionWidget(
      {super.key, required this.folders, this.spacing = 10.0});

  Widget _buildSubSection(MenuModel folder, int depth) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: depth * spacing, bottom: 5),
          child: // MenuItemWidget()
              Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                folder.icon,
                color: AppColors.mainColor,
                size: 15,
              ),
              const SizedBox(width: 2),
              Text(folder.title,
                  style: TextStyle(fontSize: 12, color: AppColors.mainColor)),
            ],
          ),
        ),
        if (folder.children != null && folder.children!.isNotEmpty)
          ...folder.children!.map((child) => SingleChildScrollView(
                child: _buildSubSection(child, depth + 1),
              )),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: folders.length,
      itemBuilder: (context, index) {
        return _buildSubSection(folders[index], 0);
      },
    );
  }
}
